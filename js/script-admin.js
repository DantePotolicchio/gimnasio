class User {
  constructor(name, password) {
    this.name = name;
    this.password = password;
    this.clases = [];
    this.status = true;
  }
}

class Clase {
  constructor(nombre, cupo, inicio, fin, id = generarId(10)) {
    this.id = id;
    this.nombre = nombre;
    this.cupo = cupo;
    this.inicio = inicio;
    this.fin = fin;
  }
}

function generarId(long) {
  let id = '', caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', caracteresLength = caracteres.length;
  for (let i = 0; i < long; i++)
    id += caracteres.charAt(Math.floor(Math.random() * caracteresLength));
  return id;
}

function msg(icon, title, txt = "", position = "center") {
  Swal.fire({
    icon: icon,
    title: title,
    text: txt,
    position: position,
    showConfirmButton: false,
    showClass: {
      popup: 'animated fadeInDown faster'
    },
    hideClass: {
      popup: 'animated fadeOutUp faster'
    },
    timer: 1500
  });
}

function logOut() {
  localStorage.setItem('currentUser', JSON.stringify(new User('', '')));
  location.href = './index.html';
}

function crearUsuario() {
  let users = JSON.parse(localStorage.getItem('users'));
  Swal.fire({
    title: 'Crear Usuario',
    icon: 'info',
    input: 'text',
    inputPlaceholder: 'Ingrese el nombre de Usuario',
    inputAttributes: {
      maxlength: 30,
      autocapitalize: 'off',
      autocorrect: 'off',
      autocomplete: 'off'
    },
    showCloseButton: true,
    showCancelButton: true,
    cancelButtonText: "Cancelar",
    //cancelButtonColor: "black",
    confirmButtonText: "Crear",
    showClass: {
      popup: 'animated fadeInDown faster'
    },
    inputValidator: (value) => {
      return new Promise((resolve) => {
        let user = value.toLowerCase();
        if (user) {
          user = user.toLowerCase();
          if (users.find(u => u.name == user))
            resolve('*Nombre de Usuario en Uso');
          else
            resolve();
        }
        else
          resolve('*El nombre de usuario no puede estar vacío');
      });
    }
  }).then((result) => {
    if (!result.dismiss) {
      users.push(new User(result.value.toLowerCase(), result.value.toLowerCase()));
      msg('success', 'Usuario Creado Correctamente', "", "top-end");
      let admin = users.shift();
      users.sort((a, b) => a.name.localeCompare(b.name));
      users.unshift(admin);
      localStorage.setItem('users', JSON.stringify(users));
      listarUsuarios();
    }
  });
}

function cancelarInscripcion(id, username) {
  let users = JSON.parse(localStorage.getItem('users')), user = users.findIndex(u => u.name == username);
  Swal.fire({
    title: `<h2>¿Está seguro que desea quitar a 
    <b class="d-inline-flex text-capitalize">${username}
    </b> de <b class="d-inline-flex text-capitalize">${users[user].clases.find(c => c.id == id).nombre}</b>?</h2>`,
    icon: "warning",
    showCancelButton: true,
    // cancelButtonColor: "black",
    confirmButtonColor: "red",
    confirmButtonText: "Borrar",
    showClass: {
      popup: 'animated fadeInDown faster'
    },
    hideClass: {
      popup: 'animated fadeOutUp faster'
    }
  }).then((result) => {
    if (result.value) {
      users[user].clases.splice(users[user].clases.findIndex(c => c.id == id), 1);
      msg('success', 'Listo!', 'Se canceló la inscripción');
      localStorage.setItem('users', JSON.stringify(users));
      cargar();
    }
  });
}

function detallesUser(user) {
  if (user.clases.length > 0) {
    let clases = user.clases.sort((a, b) => moment(a.incio) - moment(b.inicio)), detalle = '<ul class="pl-3">';
    for (let i = 0; i < clases.length && i < 10; i++) {
      let clase = clases[i];
      detalle += 
      `<li>
        <ol class="breadcrumb bg-white shadow align-items-center border-bottom border-top">
          <li class="text-capitalize text-success"><i class="far fa-calendar-alt mr-2"></i>${clase.nombre}</li>
          <li class="ml-auto">${moment(clase.inicio).format("ddd D MMM YYYY")} de ${moment(clase.inicio).format("HH:mm")} a ${moment(clase.fin).format("HH:mm")}</li>
          <li class="ml-auto justify-content-end">
            <button class="btn btn-outline-primary mr-2 mr-md-3" onclick="detallesClase('${clase.id}')"><i class="fas fa-bars"></i><span class="d-none d-md-inline-flex  ml-0 ml-md-2">Detalles</span></button>
            <button class="btn btn-outline-danger" onclick="cancelarInscripcion('${clase.id}', '${user.name}')"><i class="fas fa-times mr-2"></i><span class="d-none d-md-inline-flex ml-0 ml-md-2">Cancelar</span></button>
          </li>
        </ol>
      </li>`;
    }
    return detalle + '</ul>';
  }
  else
    return `<h6>Este usuario aún no asistió a ninguna clase</h6>`;
}

function listarUsuarios(users = JSON.parse(localStorage.getItem('users'))) {
  let tabla1 = "", tabla2 = "";
  if (users.length < 2)
    tabla1 += 
    `<ol class="breadcrumb bg-white shadow align-items-center">
      <i class="fas fa-exclamation-circle mr-2"></i><b class="font-weight-bold">Aún no existen usuarios registrados.</b>
    </ol>`;
  else
    for (let i = 1; i < users.length; i++) {
      let user = users[i];
      if (user.status)
        tabla1 +=
        `<ol class="breadcrumb bg-white shadow align-items-center border-bottom">
          <li class="text-primary text-capitalize" role="button" data-toggle="collapse" data-target="#collapseUser${i}" aria-expanded="false" aria-controls="collapseNota${i}"><i class="fas fa-user mr-2"></i>${user.name}</li>
          <li class="ml-auto">
            <button class="btn btn-outline-primary" onclick="editarUsuario(${i})"><i class="fas fa-edit"></i><span class="d-none d-md-inline-flex  ml-0 ml-md-2">Editar</span></button>
            <button class="btn btn-outline-warning mx-2 mx-md-3" onclick="suspenderUsuario(${i})"><i class="far fa-pause-circle"></i><span class="d-none d-md-inline-flex  ml-0 ml-md-2">Suspender</span></button>
            <button class="btn btn-outline-danger" onclick="eliminarUsuario(${i})"><i class="fas fa-user-times"></i><span class="d-none d-md-inline-flex  ml-0 ml-md-2">Borrar</span></button>
          </li>
          <li class="collapse w-100 pt-3" id="collapseUser${i}">
            ${detallesUser(user)}
          </li>
        </ol>`;
      else
        tabla2 +=
        `<ol class="breadcrumb bg-white shadow align-items-center border-bottom">
          <li class="text-warning text-capitalize"><i class="fas fa-user mr-2"></i>${user.name}</li>
          <li class="ml-auto">
            <button class="btn btn-outline-primary" onclick="editarUsuario(${i})"><i class="fas fa-edit"></i><span class="d-none d-md-inline-flex  ml-0 ml-md-2">Editar</span></button>
            <button class="btn btn-outline-success mx-2 mx-md-3" onclick="suspenderUsuario(${i})"><i class="far fa-check-circle"></i><span class="d-none d-md-inline-flex  ml-0 ml-md-2">Habilitar</span></button>
            <button class="btn btn-outline-danger" onclick="eliminarUsuario(${i})"><i class="fas fa-user-times"></i><span class="d-none d-md-inline-flex  ml-0 ml-md-2">Borrar</span></button>
          </li>
        </ol>`;
    }
  document.getElementById("usuarios").innerHTML =  (tabla1 ? tabla1 + '<small class="text-muted">Haga click sobre el nombre de un usuario para ver más detalles</small>' : 
    `<ol class="breadcrumb bg-white shadow align-items-center border-bottom">
      <b class="font-weight-bold">No hay usuarios activos.</b>
    </ol>`) + (tabla2 ? `<h1 class="mt-4 mt-md-5 mb-3 text-warning">Usuarios Suspendidos</h1>` + tabla2 : '');
}

function editarUsuario(index) {
  document.getElementById('editUserModal').dataset.index = index;
  $('#editUserModal').modal('show');
}

function suspenderUsuario(index) {
  let users = JSON.parse(localStorage.getItem('users')), action = "habilitar", advice = "", confirmColor = "#28a745", msgTxt = "El usuario fue habilitado!";
  if (users[index].status) {
    action = "suspender";
    advice = "El usuario será dado de baja en todas las clases";
    confirmColor = '#ffc107';
    msgTxt = 'El usuario fue suspendido!';
  }
  Swal.fire({
    title: `<h2>¿Está seguro que desea ${action} a <b class="d-inline-flex text-capitalize">${users[index].name}</b>?</h2>`,
    text: advice,
    icon: "warning",
    showCancelButton: true,
    cancelButtonColor: "#6c757d",
    cancelButtonText: "Cancelar",
    confirmButtonColor: confirmColor,
    confirmButtonText: `<b class="text-capitalize">${action}</b>`,
    showClass: {
      popup: 'animated fadeInDown faster'
    },
    hideClass: {
      popup: 'animated fadeOutUp faster'
    }
  }).then((result) => {
    if (result.value) {
      users[index].status = !users[index].status;
      users[index].clases = users[index].clases.filter(c => moment(c.fin) < moment());
      localStorage.setItem('users', JSON.stringify(users));
      msg('success', msgTxt, "", "top-end");
      listarUsuarios();
      listarClases();
    }
  });
}

function eliminarUsuario(index) {
  let users = JSON.parse(localStorage.getItem('users'));
  Swal.fire({
    title: `<h2>¿Está seguro que desea eliminar a <b class="d-inline-flex text-capitalize">${users[index].name}</b>?</h2>`,
    icon: "warning",
    showCancelButton: true,
    // cancelButtonColor: "black",
    confirmButtonColor: "red",
    confirmButtonText: "Borrar",
    showClass: {
      popup: 'animated fadeInDown faster'
    },
    hideClass: {
      popup: 'animated fadeOutUp faster'
    }
  }).then((result) => {
    if (result.value) {
      users.splice(index, 1);
      localStorage.setItem('users', JSON.stringify(users));
      msg('success', "Hecho!", "Usuario eliminado.", "top-end");
      listarUsuarios();
    }
  });
}

function listarClases(clases = JSON.parse(localStorage.getItem('clases'))) {
  let tabla1 = "", tabla2 = "";
  if (!clases || clases.length < 1)
    tabla1 += '<ol class="breadcrumb bg-white shadow align-items-center"><b class="font-weight-bold">No existen clases.</b></ol>';
  else {
    let now = moment(), proximas = clases.filter(clase => moment(clase.fin) > now).sort((a, b) => moment(a.inicio) - moment(b.inicio)),
    historial =  clases.filter(clase => moment(clase.fin) < now).sort((a, b) => moment(a.inicio) - moment(b.inicio));
    if (proximas)
      for (let i = 0; i < proximas.length; i++) {
        let clase = proximas[i];
        tabla1 += 
        `<ol class="breadcrumb bg-white shadow align-items-center border-bottom">
          <li class="text-capitalize text-success"><i class="far fa-calendar-alt mr-2"></i>${clase.nombre}</li>
          <li class="ml-auto">Inscriptos: ${cantidadInscriptos(clase.id)} / ${clase.cupo}</li>
          <li class="ml-auto">${moment(clase.inicio).format("ddd D MMM YYYY")}<br>de ${moment(clase.inicio).format("HH:mm")} a ${moment(clase.fin).format("HH:mm")}</li>
          <li class="ml-auto justify-content-end">
            <button class="btn btn-outline-primary mr-2 mr-md-3" onclick="editarClase('${clase.id}')"><i class="fas fa-edit"></i><span class="d-none d-md-inline-flex  ml-0 ml-md-2">Editar</span></button>
            <button class="btn btn-outline-danger" onclick="eliminarClase('${clase.id}')"><i class="far fa-calendar-times"></i><span class="d-none d-md-inline-flex  ml-0 ml-md-2">Borrar</span></button>
          </li>
         </ol>`;
      }
    if (historial)
      for (let i = 0; i < historial.length; i++) {
        let clase = historial[i];
        tabla2 += 
        `<ol class="breadcrumb bg-white shadow align-items-center border-bottom">
          <li class="text-capitalize text-secondary"><i class="far fa-calendar-check mr-2"></i>${clase.nombre}</li>
          <li class="ml-auto">Inscriptos: ${cantidadInscriptos(clase.id)} / ${clase.cupo}</li>
          <li class="ml-auto">${moment(clase.inicio).format("ddd D MMM YYYY")}<br>de ${moment(clase.inicio).format("HH:mm")} a ${moment(clase.fin).format("HH:mm")}</li>
          <li class="ml-auto justify-content-end">
            <button class="btn btn-outline-primary mr-2 mr-md-3" onclick="detallesClase('${clase.id}')"><i class="fas fa-bars"></i><span class="d-none d-md-inline-flex  ml-0 ml-md-2">Detalles</span></button>
            <button class="btn btn-outline-danger" onclick="eliminarClase('${clase.id}')"><i class="far fa-calendar-times"></i><span class="d-none d-md-inline-flex  ml-0 ml-md-2">Borrar</span></button>
          </li>
         </ol>`;
      }
  }
  $("#clases").html((tabla1 ? tabla1 : 
    `<ol class="breadcrumb bg-white shadow align-items-center">
      <b class="font-weight-bold">No hay clases próximas.</b>
    </ol>`) + (tabla2 ? `<h1 class="text-secondary mt-4 mt-md-5">Historial de Clases</h1>` : '') + tabla2);
}

function obtenerInscriptos(id) {
  let users = JSON.parse(localStorage.getItem('users')), inscriptos = '';
  for(let i = 0; i < users.length; i++)
    if (users[i].clases.find(c => c.id = id))
      inscriptos += `<ol class="breadcrumb bg-white align-items-center mb-0 border"><li class="text-capitalize mx-auto">${users[i].name}</li></ol>`;
  return inscriptos;
}

function cantidadInscriptos(id) {
  let users = JSON.parse(localStorage.getItem('users')), inscriptos = 0;
  for(let i = 0; i < users.length; i++)
    if (users[i].clases.find(clase => clase.id == id))
      inscriptos++;
  return inscriptos;
}

function editarClase(id) {
  document.getElementById('classModal').dataset.id = id;
  $('#classModal').modal('show');
}

function detallesClase(id) {
  document.getElementById('detailClass').dataset.id = id;
  $('#detailClass').modal('show');
}

function eliminarClase(id) {
  let clases = JSON.parse(localStorage.getItem('clases')), clase = clases.find(c => c.id == id);
  if (clase)
    Swal.fire({
      title: `<h2>¿Está seguro que desea eliminar la clase <b class="d-inline-flex text-capitalize">${clase.nombre}</b>?</h2>`,
      icon: "warning",
      showCancelButton: true,
      cancelButtonColor: "black",
      confirmButtonColor: "red",
      confirmButtonText: "Borrar",
      showClass: {
        popup: 'animated fadeInDown faster'
      },
      hideClass: {
        popup: 'animated fadeOutUp faster'
      }
    }).then((result) => {
      if (result.value) {
        let users = JSON.parse(localStorage.getItem('users')), index = clases.findIndex(c => c.id == id), b = false;
        clases.splice(index, 1);
        for (let i = 0; i < users.length; i++)
          if ((idx = users[i].clases.findIndex(c => c.id == id)) > -1) {
            users[i].clases.splice(idx, 1);
            b = true;
          }
        msg('success', "Hecho!", "Clase eliminada.", "top-end");
        if (b) {
          localStorage.setItem('users', JSON.stringify(users));
          listarUsuarios();
        }
        localStorage.setItem('clases', JSON.stringify(clases));
        listarClases();
      }
    });
  else
    msg('error', 'No se pudo encontrar la clase');
}

function cargar() {
  if (!JSON.parse(localStorage.getItem('clases')))
    localStorage.setItem('clases', JSON.stringify([]));
  let currentUser = JSON.parse(localStorage.getItem('currentUser'));
  if (!currentUser || currentUser.name != "admin")
    Swal.fire({
      title: "Primero debe iniciar sesión como Administrador",
      icon: "warning",
      confirmButtonText: "Aceptar",
      showClass: {
        popup: 'animated fadeInDown faster'
      },
      hideClass: {
        popup: 'animated fadeOutUp faster'
      },
      allowOutsideClick: false,
      allowEscapeKey: false
    }).then((result) => logOut());
  else {
    listarUsuarios();
    listarClases();
  }
}

$(function () {
  cargar();

  let datos, mensajes, id, msgFecha = $('#msg-fechaClase'), btn = $('#btn-actionClase');

  $('#classModal').on('show.bs.modal', () => {
    datos = $(".classModalData");
    id = document.getElementById('classModal').dataset.id;
    mensajes = $(".classModalMsg");
    if (!id) {
      $('#classModalLabel').html("Crear Clase");
      datos[0].value = `Clase${JSON.parse(localStorage.getItem('clases')).length + 1}`;
      datos[1].value = moment().format("YYYY-MM-DD");
      datos[1].min = datos[1].value;
      datos[2].value = moment().add(1, 'h').minutes(0).format("HH:mm");
      datos[3].value = moment().add(2, 'h').minutes(0).format("HH:mm");
      datos[4].value = "20";
      msgFecha.html(moment().format("dddd D - MMMM YYYY"));
      btn.html('<i class="fas fa-plus mr-2"></i>Crear');
    }
    else {
      let clase = JSON.parse(localStorage.getItem('clases')).find(c => c.id == id), inicio = moment(clase.inicio);
      $('#classModalLabel').html("Editar Clase");
      datos[0].value = clase.nombre;
      datos[1].value = inicio.format("YYYY-MM-DD");
      datos[1].min = moment().format("YYYY-MM-DD");
      datos[2].value = inicio.format("HH:mm");
      datos[3].value = moment(clase.fin).format("HH:mm");
      datos[4].value = clase.cupo;
      msgFecha.html(inicio.format("dddd D - MMMM YYYY"));
      btn.html('<i class="fas fa-edit mr-2"></i>Modificar');
    }
    for (let i = 0; i < 5; i++)
      mensajes[i].innerHTML = '';
  });

  $('#classModal').on('shown.bs.modal', () => {
    modalClase();
  });

  btn.on('click', () => {
    if (document.getElementById('classModal').dataset.id)
      editClase();
    else
      agregarClase();
  });

  function modalClase() {
    datos = $(".classModalData");
    id = document.getElementById('classModal').dataset.id;
    mensajes = $(".classModalMsg");

    $(datos[1]).on('input', () => {
      let fecha = moment(datos[1].value);
      if (fecha.diff(moment(), 'days') < 0 || !fecha.isValid()) {
        mensajes[1].innerHTML = '*Fecha Inválida';
        msgFecha.html('');
        btn.prop('disabled', true);
      }
      else {
        if (fecha.diff(moment(), 'days') == 0)
          datos[2].value = moment().add(1, 'h').minute(0).format("HH:mm");
        else
          datos[2].value = moment().hours(6).minutes(0).format("HH:mm");
        datos[3].value = fecha.hours(datos[2].value.slice(0, 2)).minutes(0).add(1, 'h').format("HH:mm");
        mensajes[1].innerHTML = '';
        btn.prop('disabled', false);
        msgFecha.html(moment(datos[1].value).format("dddd D - MMMM YYYY"));
      }
    });

    $(datos[2]).on('input', () => {
      let horaInicio = moment(datos[1].value).hours(datos[2].value.slice(0, 2)).minutes(datos[2].value.slice(3));
      if(moment(horaInicio).diff(moment(), 'minutes') < 0) {
        mensajes[2].innerHTML = '*No puede ser un horario del pasado';
        btn.prop('disabled', true);
      }
      else {
        mensajes[2].innerHTML = '';
        btn.prop('disabled', false);
      }
      datos[3].value = horaInicio.add(1, 'h').format('HH:mm');
    });
    
    $(datos[3]).on('input', () => {
      if (moment().hours(datos[3].value.slice(0, 2)).minutes(datos[3].value.slice(3)).diff(moment().hours(datos[2].value.slice(0, 2)).minutes(datos[2].value.slice(3)), 'minutes') < 5) {
        mensajes[3].innerHTML = '*Debe ser posterior a la hora de inicio';
        btn.prop('disabled', true);
      }
      else {
        mensajes[3].innerHTML = '';
        btn.prop('disabled', false);
      }
    });
    
    $(datos[4]).on('input', () => {
      if (cantidadInscriptos(id) > datos[4].value) {
        mensajes[4].innerHTML = '*Debe ser mayor a la cantidad de inscriptos';
        btn.prop('disabled', true);
      }
      else {
        mensajes[4].innerHTML = '';
        btn.prop('disabled', false);
      }
    });
  }

  function agregarClase() {
    let clases = JSON.parse(localStorage.getItem('clases')) || [];
    for (let i = 0; i < 5; i++) {
      if (!datos[i].value) {
        mensajes[i].innerHTML = "*Campo requerido";
        clases = null;
      }
      if (mensajes[i].value && mensajes[i].value.indexOf("*") > -1)
        clases = null;
    }
    if (clases) {
      let fecha = datos[1].value.split("-");
      fecha[1]--;
      clases.push(new Clase(datos[0].value, datos[4].value, new Date(...fecha, ...datos[2].value.split(":")), new Date(...fecha, ...datos[3].value.split(":"))));
      msg("success", "Clase Creada", "", "top-end");
      localStorage.setItem('clases', JSON.stringify(clases));
      $('#classModal').modal('hide');
      listarClases();  
    }
  }
  
  function editClase() {
    let clases = JSON.parse(localStorage.getItem('clases')) || []
    for (let i = 0; i < 5; i++) {
      if (!datos[i].value) {
        mensajes[i].innerHTML = "*Campo requerido";
        clases = null;
      }
      if (mensajes[i].value && mensajes[i].value.indexOf("*") > -1)
        clases = null;
    }
    if (clases) {
      let fecha = datos[1].value.split("-"), users = JSON.parse(localStorage.getItem('users')), index = clases.findIndex(c => c.id == id), b = false,
      clase = new Clase(datos[0].value, datos[4].value, new Date(...fecha, ...datos[2].value.split(":")), new Date(...fecha, ...datos[3].value.split(":")), id);
      fecha[1]--;
      clases.splice(index, 1, clase);
      for (let i = 0; i < users.length; i++)
        if ((idx = users[i].clases.findIndex(c => c.id == id)) > -1) {
          users[i].clases.splice(idx, 1, clase);
          b = true;
        }
      msg("success", "Clase Editada", "", "top-end");
      $('#classModal').modal('hide');
      if (b) {
        localStorage.setItem('users', JSON.stringify(users));
        listarUsuarios();
      }
      localStorage.setItem('clases', JSON.stringify(clases));
      listarClases();
    }
  }

  $('#classModal').on('hidden.bs.modal', () => id = "");

  $('#detailClass').on('show.bs.modal', () => {
    datos = $(".detailClassData");
    id = document.getElementById('detailClass').dataset.id;
    mensajes = $(".detailClassMsg");
    let clase = JSON.parse(localStorage.getItem('clases')).find(c => c.id == id);
    $('#detailClassLabel').html(`Detalles <b class="text-capitalize">${clase.nombre}</b>`);
    datos[0].value = clase.nombre;
    datos[1].value = moment(clase.inicio).format("YYYY-MM-DD");
    datos[2].value = moment(clase.inicio).format("HH:mm");
    datos[3].value = moment(clase.fin).format("HH:mm");
    $('#msg-fechaDetalleClase').html(moment(clase.inicio).format("dddd D - MMMM YYYY"));
    $('#collapseInscriptos').html(obtenerInscriptos(id));
  });

  $('#editUserModal').on('show.bs.modal', () => {
    let user = JSON.parse(localStorage.getItem("users"))[document.getElementById('editUserModal').dataset.index];
    mensajes = $('.editUserModalMsg');
    datos = $('.editUserModalData');
    datos[0].value = user.name;
    datos[1].value = user.password;
  });

  $('#editUserModal').on('shown.bs.modal', () => {
    let users = JSON.parse(localStorage.getItem("users")), btn = $('#btn-editarUsuario'), 
    index = document.getElementById('editUserModal').dataset.index;
    $(datos[0]).on('input', () => {
      if (users.find((u, i) => u.name == datos[0].value.toLowerCase() && i != index)) {
        mensajes[0].innerHTML = '*nombre de usuario en uso';
        btn.prop('disabled', true);
      }
      else {
        mensajes[0].innerHTML = '';
        btn.prop('disabled', false);
      }
    });

    $('#btn-editarUsuario').on('click', function editarUsuario() {
      if (!mensajes[0].innerHTML) {
        users[index].name = datos[0].value.toLowerCase();
        users[index].password = datos[1].value;
        let admin = users.shift();
        users.sort((a, b) => a.name.localeCompare(b.name));
        users.unshift(admin);
        localStorage.setItem('users', JSON.stringify(users));
        msg('success', 'Usuario Modificado');
        listarUsuarios();
        $('#editUserModal').modal('hide');
      }
    });
  });
});